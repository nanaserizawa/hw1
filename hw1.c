

#include <stdio.h>
#include<stdlib.h>
#include<string.h>

void qsort(char str[], int first, int last);
void q_sort(char word[], int left, int right);


struct algorithm{
    char sort;
    char word;
} ;


void qsort(char str[], int first, int last)
{
    int i, j;
    char x, t;
    
    x = str[(first + last) / 2];
    i = first;
    j = last;
    for(;;i++,j--) {
        while (str[i] < x) i++;
        while (x < str[j]) j--;
        if (i >= j) break;
        t = str[i];
        str[i] = str[j];
        str[j] = t;
    }
    if (first  < i - 1) qsort(str, first , i - 1);
    if (j + 1 < last) qsort(str, j + 1, last);
}


void q_sort(char word[], int left, int right)
{
    char pivot, l_hold, r_hold;
    
    l_hold = left;
    r_hold = right;
    pivot = word[left];
    while (left < right)
    {
        while ((word[right] >= pivot) && (left < right))
            right--;
        if (left != right)
        {
            word[left] = word[right];
            left++;
        }
        while ((word[left] <= pivot) && (left < right))
            left++;
        if (left != right)
        {
            word[right] = word[left];
            right--;
        }
    }
    word[left] = pivot;
    pivot = left;
    left = l_hold;
    right = r_hold;
    if (left < pivot)
        q_sort(numbers, left, pivot-1);
    if (right > pivot)
        q_sort(numbers, pivot+1, right);
}






int main(void){
    int i, j,k,n;
    FILE *fp;
    char b[16];
    
    
    printf("¥¢¥ë¥Õ¥¡¥Ù¥Ã¥È¤òÆþÎÏ¤·¤Æ¤¯¤À¤µ¤¤\n");
    for(k=0;k<16;k++){
        scanf("%c",&b[i]);
    }
    qsort(b[],0,15);
    
    
    
    struct algorithm a[9170];
    
    fp = fopen("/usr/share/dict/american-english", "r");
    if(fp == NULL) {
        printf("¥Õ¥¡¥¤¥ë¤ò³«¤¯¤³¤È¤¬½ÐÍè¤Þ¤»¤ó¤Ç¤·¤¿¡¥\n");
        return;
    }
    
    for(i=0; i<=9271; i++){
        fscanf(fp, "%lf",a[i].word);
    }
    
    fclose(fp);
    
    
    for(i=0; i<=9171; i++){
        int n,m;
        n= strlen(a[i].word);
        m=a[i].word;
        qsort(m, 0,n-1 );
        a[i].sort=m;
    }
    
    fclose(fp);
    q_sort(a[], 0, 9170);
 
    
}


/*ソート済みの単語と元の単語を持つリストを作りたいのですが単語を上手くソートする方法が思いつきません。
単語を探すアルゴリズムは与えられた文字と辞書をそれぞれソートし、先頭の文字を確認していくものを考えています。
入力された文字がソートしたらabcd...という者だった場合、aからはじまるものがあるかを考えてなければbから始まるものあるか考える。aから始まるものがあればabから始まるものがあるか・・・とかんがえました。
しかし、辞書のそれぞれの単語の先頭を比べる方法がわかりませんでした。*/
