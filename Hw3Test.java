import static org.junit.Assert.*;

import org.junit.Test;


public class Hw3Test {

	@Test
	public void Hw3test() {
		Hw3 hw3 = new Hw3();
		String out = hw3.roate("abc", 1);
		assertEquals("cab", out);
	}
	
	@Test
	public void Hw3test2() {
		Hw3 hw3 = new Hw3();
		String out = hw3.roate("abc", 0);
		assertEquals("abc", out);
	}
	@Test
	public void Hw3test3() {
		Hw3 hw3 = new Hw3();
		String out = hw3.roate("", 0);
		assertEquals("", out);
	}
	@Test
	public void Hw3test4() {
		Hw3 hw3 = new Hw3();
		String out = hw3.roate("abc", 9999);
		assertEquals("abc", out);
	}
	@Test
	public void Hw3test5() {
		Hw3 hw3 = new Hw3();
		String out = hw3.roate("", 9);
		assertEquals("", out);
	}
	@Test
	public void Hw3test6() {
		Hw3 hw3 = new Hw3();
		String out = hw3.roate("abc", -1);
		assertEquals("bca", out);
	}


}
